from django.urls import path
from .views import Join, sched_delete

appname = 'Story5'

urlpatterns = [
   path('', Join, name = 'Schedule'),
   path('<int:pk>',sched_delete, name = 'Delete')
]