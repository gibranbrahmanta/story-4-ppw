from django.shortcuts import render

# Create your views here.

def index(request):
	response = {}
	return render(request, 'Profile.html', response)

def experience(request):
	response = {}
	return render(request, 'Experience.html', response)

def skills(request):
	response = {}
	return render(request, 'Skills.html', response)

def portofolio(request):
	response = {}
	return render(request, 'Portofolio.html', response)

def contact(request):
	response = {}
	return render(request, 'Contact.html', response)