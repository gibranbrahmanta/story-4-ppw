from django.urls import path
from .views import index
from .views import experience
from .views import skills
from .views import portofolio
from .views import contact

appname = 'Story4'

urlpatterns = [
   path('', index, name = 'index'),
   path('Experience/', experience, name = 'experience'),
   path('Skills/', skills, name = 'skills'),
   path('Portofolio/', portofolio, name = 'portofolio'),
   path('Contact/', contact, name = 'contact')
]
